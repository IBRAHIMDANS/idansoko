import { Users } from '../../types';
import { users } from '../data/users.js';
import { body } from 'express-validator';

class UsersController {

  private readonly _users: Users[];

  constructor() {
    this._users = users;
  }

  get users(): Users[] {
    return this._users;
  }

  get(): Users[] {
    return this.users;
  }

  getById(id: number): Users | undefined {
    return this.users.find(user => user.id === id);
  }

}

export default new UsersController();
