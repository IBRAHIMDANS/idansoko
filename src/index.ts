(async () => {
  const { default: Server } = await import('./libs/express.js');
  Server.start();
})();
