import express, { Express } from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import cacheControl from 'express-cache-controller';
import http from 'http';
import { Logger, morganMiddleware } from './logger.js';
import routes from '../routes/index.js';
import dotenv from 'dotenv';

dotenv.config();

export class Server {
  app: Express;

  static PORT = process.env.PORT || 3001;

  constructor() {
    this.app = express();
  }

  private config() {
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(cors({
      origin: '*',
      credentials: true,
    }));
    this.app.use(helmet());
    this.app.use(cacheControl({ noCache: true }));
    this.app.use(morganMiddleware);
  }

  //
  private routes() {
    this.app.use('/api', routes);
  }

  public start() {
    const appServer = new Server();
    appServer.config();
    appServer.routes();
    const server = http.createServer(appServer.app);
    server.listen(Server.PORT, () => {
      Logger.info(`⚡️SERVER running on port ${Server.PORT}!`);
    });
  }

}

export default new Server();
