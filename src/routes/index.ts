import { Router } from 'express';
import UsersRoutes from './user.routes.js';

const router = Router();

router.use('/', UsersRoutes);

export default router;
