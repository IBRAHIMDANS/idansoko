import { Router } from 'express';
import UsersController from '../controllers/users.controller.js';
import { body, validationResult } from 'express-validator';

const router = Router();

router.get('/users/', (req, res) => {
  const users = UsersController.get();
  return res.status(200).json(users);
});
router.get('/user/:id', (req, res) => {
  const id = Number(req.params.id);
  const user = UsersController.getById(id);
  if (user) {
    return res.status(200).json(user);
  }
  return res.status(404).json({ status: 404, message: 'user not found' });
});

router.post('/validate/user',
  body('email', 'Invalid email').exists().isEmail(),
  body('username', 'Invalid username').exists().isString(),
  body('website', 'Invalid website').exists().isString(),
  body('phone', 'Invalid phone').exists().isString(),
  body('username', 'Invalid username').exists().isString(),
  body('address.street', 'Invalid address.street').exists().isString(),
  body('address.suite', 'Invalid address.suite').exists().isString(),
  body('address.city', 'Invalid address.city').exists().isString(),
  body('address.zipcode', 'Invalid address.zipcode').exists().isString(),
  body('address.geo.lat', 'Invalid address.geo.lat').exists().isString(),
  body('address.geo.lng', 'Invalid address.geo.lng').exists().isString(),
  body('company.name', 'Invalid company.name').exists().isString(),
  body('company.catchPhrase', 'Invalid company.catchPhrase').exists().isString(),
  body('company.bs', 'Invalid company.bs').exists().isString(),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    return res.status(200).json(req.body);
  });

export default router;
