export interface Address {
  zipcode: string;
  geo: {
    lng: string;
    lat: string
  };
  suite: string;
  city: string;
  street: string;
}

export interface Company {
  bs: string;
  catchPhrase: string;
  name: string;
}

export type Users = {
  id: number;
  email: string;
  username: string
  website: string;
  phone: string;
  name: string;
  address: Address;
  company: Company;
};

const a = {

}
